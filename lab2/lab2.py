from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from Crypto.Util import number
from Crypto import Random

import random
import math
#Part 1
prime = int("B10B8F96A080E01DDE92DE5EAE5D54EC52C99FBCFB06A3C69A6A9DCA52D23B616073E28675A23D189838EF1E2EE652C013ECB4AEA906112324975C3CD49B83BFACCBDD7D90C4BD7098488E9C219A73724EFFD6FAE5644738FAA31A4FF55BCCC0A151AF5F0DC8B4BD45BF37DF365C1A65E68CFDA76D4DA708DF1FB2BC2E4A4371", 16)
gen = int("A4D1CBD5C3FD34126765A442EFB99905F8104DD258AC507FD6406CFF14266D31266FEA1E5C41564B777E690F5504F213160217B4B01B886A5E91547F9E2749F4D7FBD7D3B9A92EE1909D0D2263F80A76A6A24C087A091F531DBF0A0169B6A28AD662A4D18E73AFA32D779D5918D08BC8858F4DCEF97C2A24855E6EEB22B3B2E5", 16) 
arand = random.randint(1, prime-1)
brand = random.randint(1, prime-1)


def sharednum(g, rand, p):
    return pow(g, rand, p)

def sgen(shared, rand, p):
    return pow(shared, rand, p)

def keygen(s):
    h = SHA256.new(str(s).encode())
    hbytes = h.digest()
    return hbytes[0 : 16]

def padding(msg):
    padmsg = msg
    
    pad = 16 - (len(msg) % 16)

    if(pad % 16 != 0):
        for i in range(pad):
            padmsg += str(0)

    return padmsg

def ciphergen(msg, k, iv):
    paddedmsg = padding(msg)
    #print('padded: ', len(paddedmsg))
    cipher = AES.new(bytes(k), AES.MODE_CBC, iv)
    enc = iv + cipher.encrypt(bytes(paddedmsg.encode()))

    #dec = cipher.decrypt(enc)
    print('Encrypted message: ', enc)
    print('\n')

    return enc

def cipherdec(msg, k, iv):
    cipher = AES.new(bytes(k), AES.MODE_CBC, iv)
    dec = cipher.decrypt(msg)

    print('Decoded message: ', dec[16:len(dec)])
    print('\n')
    return dec

A = sharednum(gen, arand, prime)
B = sharednum(gen, brand, prime)

sa = sgen(B, arand, prime)
sb = sgen(A, brand, prime)

#print(sa)
#print(sb)

akey = keygen(sa)
bkey = keygen(sb)

print('-----Part 1-----')

#print('akey: ', akey)
#print('bkey: ', bkey)

iv = Random.new().read(AES.block_size)

acipher = ciphergen('Hello there, Bob you are awesome!', akey, iv)
bdecode = cipherdec(acipher, bkey, iv)
bcipher = ciphergen('Hello Alice, you are too!', bkey, iv)
adecode = cipherdec(bcipher, akey, iv)

#Part 2
def mitm(shared, p):
    shared = p
    return shared

print('-----Part 2-----')
A2 = sharednum(gen, arand, prime)
B2 = sharednum(gen, brand, prime)

A2mal = mitm(A2, prime)
B2mal = mitm(B2, prime)

samal = sgen(B2mal, arand, prime)
sbmal = sgen(A2mal, brand, prime)

akey2 = keygen(samal)
malkey = keygen(sbmal)

acipher2 = ciphergen('Hi Bob, I hope Mallory is not listening', akey2, iv)
maldec = cipherdec(acipher2, malkey, iv)

#Part 3
def primegen(numbits, e):
    p = number.getPrime(numbits)
    while(math.gcd(p, e) != 1):
        p = number.getPrime(numbits)

    return p
    
def modinverse(a, b):
    t = 0
    r = b
    newt = 1
    newr = a

    while newr != 0:
        q = r // newr
        (t, newt) = (newt, t - q * newt)
        (r, newr) = (newr, r - q * newr)

    if r > 1: print('No inverse')
    if t < 0: t = t + b

    return t

def rsapkgen(e, phi, n):
    d = modinverse(e, phi)
    return (d, n)

def rsaenc(msg, e, n):
    return pow(msg, e, n)

def rsadec(c, d, n):
    return pow(c, d, n)

print('-----Part 3-----')
primesize = 2048
e = 65537

p = primegen(primesize, e)
q = primegen(primesize, e)
n = p * q

phi = (p-1) * (q-1)
sk = (e, n)
pk = rsapkgen(e, phi, n)

cipher = rsaenc(987654321, sk[0], sk[1])
dec = rsadec(cipher, pk[0], pk[1])

print('Encrypted: ', cipher)
print('Decrypted: ', dec)


#Part 4

