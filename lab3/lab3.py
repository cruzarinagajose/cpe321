from Crypto.Hash import SHA
import base64
import sys

login = {'zachary' : b'A9Z8JjwnpFPvZbKeMDNHJzM8y80=',
    'jamie' : b'44rSFJQ9qtHWTBAvrsKd5K/p2j0=',
    'ellie' : b'drwuv3DCvq6R3jONTBmOYoZDdkU=',
    'copernicus' : b'A9Z8JjwnpFPvZbKeMDNHJzM8y80=',
    'newton' : b'zfVH7Uxk5plK81z81pxCBMkiepc=',
    'clara' : b'TBlWalehaoqsORszMiPAGFTlvqo='}

hashes = []
wordlist = []

with open(sys.argv[1], 'rb') as dic:
    word = dic.readline()
    i = 0
    while word:
        wordlist.append(word.rstrip(b'\n'))
        hashes.append(base64.b64encode(SHA.new(wordlist[i]).digest()))
        word = dic.readline()
        i += 1

i = 0

for i in range(len(hashes)):
    for user in login:
        if(login[user] == hashes[i]):
            print("User: {}\tPass: {}".format(user, wordlist[i]))
