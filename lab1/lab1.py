import os
from Crypto.Cipher import AES
import sys

def otp(str1, str2):
    if (len(str1) != len(str2)):
        return "Strings are of unequal length!"
    b1 = bytearray(str1)
    b2 = bytearray(str2)
    
    for i in range(len(b1)):
        b1[i] ^= b2[i]

    hexview = bytes(b1).hex()
    #print('The cipher is ', hexview)
    return bytes(b1)


#Part 1
print('-----Part 1-----')
cipher = otp(b'Darlin dont you go', b'and cut your hair!')
print('Cipher: ', cipher.hex())

#Part 2
def keygen(numbytes):
    return os.urandom(numbytes)

def fotp(filename):
    f = open(filename, 'r')
    enc = open(filename[0 : len(filename) - 4] + '-enc.txt', 'wb')
    dec = open(filename[0 : len(filename) - 4] + '-dec.txt', 'wb')
    
    read = f.read()
    numbytes = len(read)
    #print(numbytes)
    key = keygen(numbytes)
    
    cipher = otp(read.encode('utf-8'), key)
    enc.write(cipher)
    enc.close()
    
    enc = open(filename[0 : len(filename) - 4] + '-enc.txt', 'rb')
    decode = enc.read()
    #print(len(decode))
    #print(len(key))
    
    cipher2 = otp(decode, key)
    dec.write(cipher2)

    dec.close()
    enc.close()
    f.close()

print('-----Part 2-----')
fotp('test.txt') 

#Part 3
def iotp(filename):
    f = open(filename, 'rb')
    encfile = filename[0 : len(filename) - 4] + '-enc.bmp'
    enc = open(encfile, 'wb')
    
    read = bytearray(f.read())
    bmphdr = read[0 : 54]
    bmpdata = read[54 : len(read)]

    numbytes = len(bmpdata)
    key = keygen(numbytes)
    
    cipher = otp(bmpdata, key)
    enc.write(bmphdr)
    enc.write(cipher)
    
    enc.close()
    f.close()

print('-----Part 3-----')
iotp('mustang.bmp')
iotp('cp-logo.bmp')

#Part 4
def ttp(file1, file2):
    f1 = open(file1, 'rb')
    encfile1 = file1[0 : len(file1) - 4] + '-ttp.bmp'
    enc1 = open(encfile1, 'wb')
    
    f2 = open(file2, 'rb')
    encfile2 = file2[0 : len(file2) - 4] + '-ttp.bmp'
    enc2 = open(encfile2, 'wb')

    read1 = bytearray(f1.read())
    bmphdr = read1[0 : 54]
    bmpdata1 = read1[54 : len(read1)]

    read2 = bytearray(f2.read())
    bmpdata2 = read2[54 : len(read2)]

    numbytes = len(bmpdata1)
    key = keygen(numbytes)
    
    cipher1 = otp(bmpdata1, key)
    enc1.write(bmphdr)
    enc1.write(cipher1)

    cipher2 = otp(bmpdata2, key)
    enc2.write(bmphdr)
    enc2.write(cipher2)
   
    f3 = open('xor-ttp.bmp', 'wb')
    cipher3 = otp(cipher1, cipher2)
    f3.write(bmphdr)
    f3.write(cipher3)
    f3.close()

    enc1.close()
    enc2.close()
    f1.close()
    f2.close()

print('-----Part 4-----')
ttp('mustang.bmp', 'cp-logo.bmp')

#Part 5
def chunkify(listname, n):
    for i in range(0, len(listname), n):
        yield listname[i : i + n]

def ecb(filename):
    f = open(filename, 'rb')
    encfile = filename[0 : len(filename) - 4] + '-ecb.bmp'
    enc = open(encfile, 'wb')
    
    read = bytearray(f.read())
    bmphdr = read[0 : 54]
    bmpdata = read[54 : len(read)]

    chunks = list(chunkify(bmpdata, 16))
    padding = 16 - len(chunks[len(chunks) - 1])

    if(padding != 16):
        for i in range(0, padding):
            chunks[len(chunks) - 1].append(padding)

    key = keygen(16)
    
    cipher = AES.new(key)

    enc.write(bmphdr)

    for i in range(len(chunks)):
        enc.write(cipher.encrypt(bytes(chunks[i])))
   
    enc.close()
    f.close()

def cbc(filename):
    f = open(filename, 'rb')
    encfile = filename[0 : len(filename) - 4] + '-cbc.bmp'
    enc = open(encfile, 'wb')
    
    read = bytearray(f.read())
    bmphdr = read[0 : 54]
    bmpdata = read[54 : len(read)]

    chunks = list(chunkify(bmpdata, 16))
    padding = 16 - len(chunks[len(chunks) - 1])

    if(padding != 16):
        for i in range(0, padding):
            chunks[len(chunks) - 1].append(padding)

    key = keygen(16)
    iv = keygen(16) 

    cipher = AES.new(key)

    enc.write(bmphdr)

    initchunk = otp(chunks[0], iv)
    cipherchunk = cipher.encrypt(initchunk)
    enc.write(cipherchunk)

    cipherlist = []
    cipherlist.append(cipherchunk)

    for i in range(1, len(chunks)):
        initchunk = otp(chunks[i], cipherlist[i-1])
        cipherchunk = cipher.encrypt(initchunk)
        cipherlist.append(cipherchunk)
        enc.write(cipherchunk)
   
    enc.close()
    f.close()   

print('-----Part 5-----')
ecb('mustang.bmp')
cbc('mustang.bmp')

#Part 6
key6 = keygen(16)
iv6 = keygen(16)

def enccbc(string, key, iv):
    chunks = list(chunkify(string, 16))
    padding = 16 - len(chunks[len(chunks) - 1])
    enc = ''
    
    if(padding != 16):
        for i in range(0, padding):
            chunks[len(chunks) - 1] += str(padding)

    cipher = AES.new(key)

    initchunk = otp(iv, chunks[0].encode('utf-8'))
    cipherchunk = cipher.encrypt(initchunk)
    enc += str(cipherchunk)

    cipherlist = []
    cipherlist.append(cipherchunk)

    for i in range(1, len(chunks)):
        initchunk = otp(chunks[i].encode('utf-8'), cipherlist[i-1])
        cipherchunk = cipher.encrypt(initchunk)
        cipherlist.append(cipherchunk)
        enc += str(cipherchunk)

    return enc

def deccbc(string, key, iv):
    chunks = list(chunkify(string, 16))
    dec = ''
    
    cipher = AES.new(key)

    cipherchunk = cipher.decrypt(chunks[0])
    initchunk = otp(iv, cipherchunk)
    dec += str(initchunk)

    cipherlist = []
    cipherlist.append(initchunk)

    for i in range(1, len(chunks)):
        cipherchunk = cipher.decrypt(chunks[i])
        initchunk = otp(cipherchunk, cipherlist[i-1])
        cipherlist.append(initchunk)
        dec += str(initchunk)

    return dec

def submit(string):
    newstr = 'userid=321;userdata=' + string + ';session-id=31337'

    return enccbc(newstr, key6, iv6) 

def verify(string):
    return deccbc(string, key6, iv6)

print('-----Part 6-----')
str1 = 'hello asoifnaoi nfoasin faisfn snf isiian aisnf inainf a'
encstr1 = submit(str1)
print(encstr1)

#decstr1 = verify(encstr1)

#print(decstr1)
